## General Documents

This github repository is a temporary home for some of my markdown documents.

Initially I planeed to do it all just as a regular repository, but now I am trying to do it
all in the wiki, or at least have the wiki be the one landing place.

[Linux Command Line](linux_command_line/linux_cmd.md) General information about using the command line interface on Linux systems.

[Command Line Connection](connecting/CLI-Instructions.md) How to connect to a remote Linux server, using, for example the Windows PuTTY program, or the Terminal program on a Mac.


### Contact

[Brian Moore](briangmoore@hotmail.com)
